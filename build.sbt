import com.typesafe.sbt.packager.archetypes.ServerLoader

lazy val cluster = project.in(file(".")).settings(
  name := "brightar-cluster",
  version := "1.1.0",

  scalaVersion := "2.11.7",
  scalacOptions ++= Seq("-feature"),

  libraryDependencies ++= Seq(
    "com.typesafe.akka"   %% "akka-actor"       % "2.4.1",
    "com.typesafe.akka"   %% "akka-remote"      % "2.4.1",
    "com.typesafe.akka"   %% "akka-cluster"     % "2.4.1",
    "com.typesafe.play"   %% "anorm"            % "2.4.0",

    "com.github.romix.akka" %% "akka-kryo-serialization" % "0.4.0",
    "com.fasterxml.jackson.module"  %% "jackson-module-scala" % "2.6.0-1",

    "io.netty"            % "netty-all"         % "5.0.0.Alpha2",
    "org.postgresql"      % "postgresql"        % "9.4-1201-jdbc41",
    "org.flywaydb"        % "flyway-core"       % "3.2.1",
    "com.zaxxer"          % "HikariCP"          % "2.4.3",

    "org.slf4j" % "slf4j-simple" % "1.7.13"
  ),

  daemonUser in Linux := "root",
  serverLoading in Debian := ServerLoader.Systemd,
  maintainer in Debian := "kononencheg@gmail.com"
).enablePlugins(JavaServerAppPackaging, DebianPlugin)
