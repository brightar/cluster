package brightar.cluster

import akka.actor._
import com.typesafe.config.ConfigFactory
import scala.collection.JavaConversions._

object Seed extends App {

  private val _config = ConfigFactory.load()

  _config.getConfigList("brightar.seed.apps").foreach({ app =>
    val name = app.getString("name")
    val hostname = app.getString("hostname")
    val port = app.getInt("port")
    val config = ConfigFactory.parseString(s"""
      akka.remote.netty.tcp {
        hostname = "$hostname"
        port = "$port"
      }

      akka.cluster.seed-nodes = ["akka.tcp://$name@$hostname:$port"]
    """).withFallback(_config)

    ActorSystem(name, config).actorOf(Props[Node]) ! None
  })


}

