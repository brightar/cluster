package brightar.cluster.srv

import com.typesafe.config.Config

trait Service {
  def name: String =
    this.getClass.getPackage.getName.split('.').last

  def version: String = Option(this.getClass.getPackage
    .getImplementationVersion).getOrElse("debug")

  def init(config: Config): Unit
}
