package brightar.cluster

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag

package object srv {

  trait Asset extends Serializable {
    def service: String =
      this.getClass.getPackage.getName.split('.').last
  }

  case class State(entry: ActorRef, service: String)
  
  object State {
    def apply(entry: ActorRef, service: Service): Init =
      Init(State(entry, _fullName(service.name, service.version)))
  }

  sealed trait Action
  case class Init(state: State) extends Action
  case class Accept(state: State) extends Action

  case class Call(asset: Asset, circuit: Circuit, context: Any) {
    def target: Option[String] = 
      circuit.get(asset.service).map(_fullName(asset.service, _))

    def apply(implicit timeout: Timeout): Future[_] =
      Application.cluster ? this
  }

  // TODO: Method recover for error catching
  abstract class Method[T : ClassTag] extends Asset {
    type Result = T

    def call(circuit: Circuit, context: Any): Call =
      Call(this, circuit, context)

    def apply(circuit: Circuit, context: Any)(implicit
              timeout: Timeout,
              dispatcher: ExecutionContext): Future[T] =
      call(circuit, context).apply.mapTo[T]
  }

  private def _fullName(name: String, version: String): String =
    name + "-" + version.replace("\\.", "-")
}
