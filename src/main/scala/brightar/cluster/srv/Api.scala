package brightar.cluster.srv

import akka.actor.Actor
import akka.util.Timeout
import akka.pattern.pipe
import brightar.cluster._

import scala.concurrent.Future
import scala.concurrent.duration._

abstract class Api[Context, Processor] extends Actor {
  implicit val _executionContext = context.dispatcher
  implicit val _requestTimeout: Timeout = 10.seconds

  type Process = PartialFunction[Any, Future[Any]]

  def receive: Receive = {
    case Call(method, circuit, options) =>
      val context = options.asInstanceOf[Context]
      val processor = enter(context)

      api(circuit, processor, context).applyOrElse(method, { _: Any =>
        Future.failed(new Exception(s"Unable to call api method $method"))
      }).andThen({
        case _ => exit(processor)
      }).recover({
        case exception => exit(processor)

          println(method, circuit, options)
          println(exception, exception.getStackTrace.mkString("\n"))

          throw new Exception(s"Method $method execution fail.")
      }) to sender
  }

  def api(circuit: Circuit, processor: Processor, options: Context): Process
  def enter(context: Context): Processor
  def exit(processor: Processor): Unit
}
