package brightar.cluster.client

import akka.actor.Status.Failure
import akka.actor.{Actor, ActorRef, Terminated}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}

class Proxy extends Actor {
  private val _name = self.path.name

  def receive: Receive = _reject

  override def preStart(): Unit =
    println(s"Client ${ _name } proxy started.")

  private def _reject: Receive = {
    case service: ActorRef => context become _proxy(
      Router(RoundRobinRoutingLogic()).addRoutee(context watch service))
      println(s"Client ${ _name } proxy accept first remote.")

    case msg: Any => sender ! Failure(new Exception(
      s"No service ${ _name } endpoint available."))
  }

  private def _route(router: Router): Receive = {
    case service: ActorRef => if (_canAccept(router, service)) {
      context become _proxy(router.addRoutee(context watch service))

      println(s"Client ${ _name } proxy accept remote. Least ${
        router.routees.length + 1 }.")
    }

    case Terminated(service) =>
      context become _proxy(router.removeRoutee(context unwatch service))

      println(s"Client ${ _name } proxy lost remote. Least ${
        router.routees.length - 1}.")

    case msg: Any =>
      router route (msg, sender())
  }

  private def _proxy(router: Router): Receive =
    if (router.routees.isEmpty) _reject else _route(router)

  private def _canAccept(router: Router, service: ActorRef): Boolean =
    !router.routees.contains(ActorRefRoutee(service))
}
