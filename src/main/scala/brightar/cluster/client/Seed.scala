package brightar.cluster.client

import akka.actor.Actor
import brightar.cluster.Application
import brightar.cluster.srv.{Accept, State}

class Seed extends Actor {
  def receive: Receive = {
    case state: State => try {
      Application.cluster ! Accept(state)
    } catch {
      case _: Throwable =>
    } finally {
      context stop self
    }
  }
}
