package brightar.cluster

import akka.actor.Status.Failure
import akka.actor._
import akka.cluster.ClusterEvent.{CurrentClusterState, MemberEvent, MemberUp, UnreachableMember}
import akka.cluster.{Member, MemberStatus}
import akka.pattern.{ask, pipe}
import akka.remote.RemoteScope
import akka.util.Timeout
import scala.concurrent.duration._
import brightar.cluster.client.{Proxy, Seed}
import brightar.cluster.srv.{Accept, Init, Call, State}

class Node extends Actor {
  implicit val _executionContext = context.dispatcher
  implicit val _requestTimeout: Timeout = 30.seconds

  private val _cluster = akka.cluster.Cluster(context.system)

  override def preStart(): Unit =
    _cluster subscribe(self, classOf[MemberEvent])

  override def postStop(): Unit =
    _cluster unsubscribe(self, classOf[MemberEvent])

  def receive: Receive = _announcing(Set())

  private def _announcing(current: Set[State]): Receive = {
    case None =>
      context.become(Actor.emptyBehavior, discardOld = true)

    case call @ Call(_, _, _) =>
      call.target.flatMap(context.child) match {
        case Some(ref) => ref.ask(call) to sender
        case None => sender ! Failure(new Exception(
          s"No available proxy for $call."))
      }

    case Init(state) =>
      _accept(state)
      _announce(current + state)

    case Accept(state) =>
      _accept(state)

    case Terminated(ref) =>
      _announce(_exclude(current, context.unwatch(ref)))

    case state: CurrentClusterState =>
      state.members.
        filter(_.status == MemberStatus.Up).
        foreach(_deploy(_, current))

    case MemberUp(member) =>
      _deploy(member, current)
  }

  private def _accept(state: State): Unit = {
    context.child(state.service).getOrElse(
      context.actorOf(Props[Proxy], state.service)) ! state.entry

    context.watch(state.entry)
  }

  private def _announce(state: Set[State]): Unit = {
    _cluster sendCurrentClusterState self
    _deploy(_cluster.selfAddress, state)

    context become _announcing(state)
  }

  private def _deploy(member: Member, state: Set[State]): Unit =
      _deploy(member.address, state)

  private def _deploy(address: Address, state: Set[State]): Unit =
    state.foreach(context.system.actorOf(Props[Seed].
      withDeploy(Deploy(scope = RemoteScope(address)))) ! _)

  private def _exclude(state: Set[State], ref: ActorRef) = state.filter {
    case State(`ref`, _) => false
    case _ => true
  }
}
