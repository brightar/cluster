package brightar.cluster.entry

import akka.actor.ActorSystem
import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.http.{HttpObjectAggregator, HttpServerCodec}


class Initializer(_system: ActorSystem, _protocol: Protocol, _context: Any)
  extends ChannelInitializer[SocketChannel] {

  def initChannel(channel: SocketChannel): Unit =
    channel.pipeline()
      .addLast(new HttpServerCodec())
      .addLast(new HttpObjectAggregator(1048576))
      .addLast(new Codec(_protocol, _context))
      .addLast(new Processor(_system))
}


