package brightar.cluster.entry

import akka.actor.ActorSystem
import akka.util.Timeout
import brightar.cluster.srv.Call
import io.netty.channel.{ChannelHandlerAdapter, ChannelHandlerContext}

import scala.util.control.NonFatal
import scala.concurrent.duration._

class Processor(_system: ActorSystem) extends ChannelHandlerAdapter {
  implicit val _currentSystem = _system
  implicit val _executionContext = _system.dispatcher
  implicit val _requestTimeout: Timeout = 10.seconds

  override def channelRead(ctx: ChannelHandlerContext,
                           msg: Object): Unit = msg match {
    case call @ Call(_, _, _) =>
      call.apply recover {
        case NonFatal(error) => error
      } map ctx.writeAndFlush

    case _ => ctx.close()
  }
}
