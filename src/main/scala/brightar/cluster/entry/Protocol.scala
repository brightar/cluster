package brightar.cluster.entry

import brightar.cluster.Circuit
import brightar.cluster.srv.Method

trait Protocol {
  def encode(value: Any): String
  def headers(value: Any): Iterable[(String, String)]

  def circuit(headers: Map[String, String]): Circuit

  def context(base: String): Any
  def context(headers: Map[String, String], base: Any): Any

  def method(uri: String, payload: String): Method[_]
}
