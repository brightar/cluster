package brightar.cluster.entry

import io.netty.buffer.Unpooled
import io.netty.channel._
import io.netty.handler.codec.http._
import io.netty.util.CharsetUtil

import scala.collection.JavaConversions._
import scala.util.control.NonFatal

class Codec(_protocol: Protocol, _context: Any)
  extends ChannelHandlerAdapter {

  override def write(ctx: ChannelHandlerContext,
                     msg: AnyRef, promise: ChannelPromise): Unit =
    ctx.write(_response(msg), promise)

  override def exceptionCaught(ctx: ChannelHandlerContext,
                               cause: Throwable): Unit =
    ctx.writeAndFlush(_response(cause))

  override def channelRead(ctx: ChannelHandlerContext, msg: Object): Unit =
    msg match {
      case req: FullHttpRequest => try {
        val buf = req.content()
        val content = buf.toString(CharsetUtil.UTF_8)
        val headers = req.headers.map({
          entry => entry.getKey.toString.toLowerCase -> entry.getValue.toString
        }).toMap

        val method = _protocol.method(req.uri, content)
        val circuit = _protocol.circuit(headers)
        val context = _protocol.context(headers, _context)

        ctx.fireChannelRead(method.call(circuit, context))
        buf.release()
      } catch {
        case NonFatal(err) => println(req.uri, err)
          ctx.writeAndFlush(_response(err))

        case err: Exception => println(req.uri, err)
          ctx.close()
      }

      case _ => ctx.close()
    }

  private def _response(msg: AnyRef) = {
    val data = Unpooled.copiedBuffer(_protocol.encode(msg), CharsetUtil.UTF_8)
    val response = new DefaultFullHttpResponse(
      HttpVersion.HTTP_1_1, HttpResponseStatus.OK, data)

    response.headers
      .setInt(HttpHeaderNames.CONTENT_LENGTH, data.readableBytes())
      .set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE)

    _protocol.headers(msg).foldLeft(response.headers)({
      case (headers, (name, value)) => headers.set(name, value)
    })

    response
  }
}
