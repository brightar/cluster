package brightar.cluster

import akka.actor._
import brightar.cluster.srv.Service
import brightar.cluster.entry.{Initializer, Protocol}
import brightar.protocol.JSON
import com.typesafe.config.ConfigFactory
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import scala.collection.JavaConverters._

object Application {
  private val _config = ConfigFactory.load()

  val name = _config.getString("brightar.application")
  val system = ActorSystem(name, _config)
  val cluster = system.actorOf(Props[Node])

  def run(service: Service): Unit = {
    val path = "brightar.service." + service.name

    service.init(ConfigFactory
      .parseString(s"$path = {}")
      .withFallback(_config)
      .getConfig(path))
  }

  def entry(protocol: Protocol): Unit = {
    val host = _config.getString("brightar.entry.hostname")
    val port = _config.getInt("brightar.entry.port")

    val context = protocol.context(JSON.writeValueAsString(_config
      .getObject("brightar.entry.context").asScala
      .map({ case (key, value) => key -> value.unwrapped })))

    new ServerBootstrap()
      .group(new NioEventLoopGroup(), new NioEventLoopGroup())
      .channel(classOf[NioServerSocketChannel])
      .childHandler(new Initializer(system, protocol, context))
      .option[Integer](ChannelOption.SO_BACKLOG, 128)
      .childOption[java.lang.Boolean](ChannelOption.SO_KEEPALIVE, true)
      .bind(host, port)
      .sync()
      .channel()
      .closeFuture()
      .sync()
  }
}
