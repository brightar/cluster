package brightar.storage

import java.util.Properties
import javax.sql.DataSource

import com.typesafe.config.Config
import com.zaxxer.hikari.{HikariDataSource, HikariConfig}
import org.flywaydb.core.Flyway
import org.postgresql.ds.PGSimpleDataSource

object Pool {
  def apply(app: String, service: String, config: Config): DataSource = {
    val storage = app + "-" + service

    val props = new Properties()

    props.setProperty("dataSourceClassName",
      "org.postgresql.ds.PGSimpleDataSource")

    props.setProperty("dataSource.serverName",
      config.getString("storage.host"))

    props.setProperty("dataSource.portNumber",
      config.getString("storage.port"))

    props.setProperty("dataSource.password",
      config.getString("storage.password"))

    props.setProperty("dataSource.databaseName", storage)
    props.setProperty("dataSource.user", storage)

    val hikari = new HikariConfig(props)
    hikari.setMaximumPoolSize(config.getInt("size") * 5)

    val pg = new HikariDataSource(hikari)

    val migration = new Flyway()
    migration.setDataSource(pg)
    migration.setLocations(service + "/db/migration")
    migration.migrate()

    pg
  }
}
