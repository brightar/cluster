package brightar.storage

import java.util.UUID
import javax.sql.DataSource

class Query(_pool: DataSource) {
  implicit val connection = _pool.getConnection

  def uuid: String = UUID.randomUUID().toString
  def now: Long = System.currentTimeMillis
  def destroy(): Unit = connection.close()
}
