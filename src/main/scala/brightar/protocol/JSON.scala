package brightar.protocol

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

object JSON extends ObjectMapper with ScalaObjectMapper {
  registerModule(DefaultScalaModule)
  configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
}
