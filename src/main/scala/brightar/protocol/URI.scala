package brightar.protocol

import java.net.{URLDecoder, URLEncoder}


object URI {

  def decode(query: String): Map[String, String] = {
    query.split('&').map(_.split('=')).flatMap({
      case Array(key, value) => Some(key -> URLDecoder.decode(value, "UTF-8"))
      case Array(key) => Some(key -> "")
      case _ => None
    }).toMap
  }

  def encode(source: Map[String, String],
             separator: String = "&",
             sorted: Boolean = false): String = {
    (if (sorted) source.toList.sorted else source)
      .map(a => a._1 + "=" + a._2)
      .mkString(separator)
  }

}
